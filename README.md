# What is MonkeyBadger?

MonkeyBadger is an error tracking system that uses a Bayesian classifier to send notifications when it classifies an error as urgent.
It can also learn to ignore the noise of inconsequential errors.

# How does it work?

You install a plugin into your app (we support the HoneyBadger plugin) and monkeypatch our own endpoint.  When an exception occurs,
the classifier determines how critical the error is.  Your team can respond to critical errors immediately, but can review other errors
the next day at the office.

# How does it know which errors are critical?

MonkeyBadger uses a Bayesian classifier that you train using your own exception data.  Go to the training screen and classify some exceptions.
After you have classified a hundred or so, the classifier becomes pretty accurate.  This is the same technology used by many spam filters.
