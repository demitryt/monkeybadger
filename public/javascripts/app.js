var classificationUndone = false;

$(document).ready(function(){
  $(".blob").each(function(key, val){
    var sha = val.id.split('_')[1];
    $.get('/blob/' + sha, function(data){
      $(val).html(data);
    });
  });

  $("select.classify").change(function(){
    var sha = this.id.split("_")[1];
    var classification = $(this).val();
    var el = this;
    var exceptionSection = $(el).parent().parent();
    var originalException = exceptionSection.html();

    exceptionSection.html(
      "<p>You have marked this exception as " + "<b>" + (classification === "Ignore" ? "Ignored" : classification) + "</b>. " +
       "<a href='#' id='undo_classification'><u>Undo</u></a>" + "</p>");

    $('body').on('click', "#undo_classification", function() {
      exceptionSection.html(originalException);
      classificationUndone = true;
      // need to reload so that the select becomes functional again for same exception
      location.reload();
    });

    // User has 4 seconds to undo classification of exception
    setTimeout(function() {
      if (classificationUndone === false ) {
        $.get("/classify/" + sha + "/" + classification);
        exceptionSection.fadeOut();
        var count = parseInt($("#count").html());
        $("#count").html(count - 1);
      }
    }, 4000);
  });
});
